import React from 'react';
import ReactDOM from 'react-dom/client';
import { imageList } from './image-list';
import './index.css';

class Header extends React.Component {
  render() {
    return (
      <h1>{this.props.title ? this.props.title : 'Header title'}</h1>
    )
  }
}

class Photos extends React.Component {
  render() {
    return (
      <React.Fragment>
        <img src={this.props.url} alt={this.props.children} width='200'/>
      </React.Fragment>
    )
  }
}

class Main extends React.Component {
  constructor() {
    super();
    this.imageList = imageList.filter((element) => element.id <= 10);
  }

  render() {
    return (
      <ul>{this.imageList.map((element) => {
        return (
          <li key={element.id.toString()} style={{display: 'inline-block'}}>
            <Photos url={element.url}>{element.title}</Photos>
          </li>)
      })}</ul>
    )
  }
}

class Footer extends React.Component {
  render() {
    return (
      <h3>{this.props.title ? this.props.title : 'Footer title'}</h3>
    )
  }
}


class Page extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Header title='Header'></Header>
        <Main></Main>
        <Footer title='Footer'></Footer>
      </React.Fragment>
    )
  }
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.Fragment>
    <Page />
  </React.Fragment>
);

